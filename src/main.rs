use std::io;
use std::fs::OpenOptions;
use std::io::Write;

use std::process::{Command, Stdio};

fn main() {
    rosh_loop();
}

fn rosh_loop() {
    loop {
        // Print the prompt
        print!(">");
        io::stdout().flush().unwrap();

        // Get the line, turn it into arguments, and execute them
        let line = get_line();
        let args = split_line(&line);
        // let status = rosh_execute(args);

        println!("{}", line);
        println!("{:?}", args);

        rosh_launch(args);

        // if !status {
        //     break
        // }
    }
}

fn get_line() -> String {
    let mut buffer: String = String::new();
    io::stdin()
        .read_line(&mut buffer)
        .unwrap();

    let cleaned_buffer = buffer.replace('\n', "");

    add_rosh_history(&cleaned_buffer);

    return cleaned_buffer;
}

fn split_line(line: &String) -> Vec<&str> {
    line.split(' ').collect::<Vec<&str>>()
}

fn rosh_execute(args: Vec<&str>) -> i32 {
    0
}

fn rosh_launch(args: Vec<&str>) {
    let used_args: Vec<&str>;
    if args[0] == "" {
        used_args = args[1..].to_owned();
    } else {
        used_args = args;
    }

    println!("Command: {}, args: {:?}", used_args[0], &used_args[1..]);

    match Command::new(used_args[0])
        .args(&used_args[1..])
        .stdin(Stdio::inherit())
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .output()
        //.expect("Failed to execute command");
    {
        Err(why) => println!("Failed to execute command: {}", why),
        Ok(..) => {}
    }
}

fn add_rosh_history(line: &String) {
    if line.as_bytes()[0] as char != ' ' {
        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .create(true)
            .open(".rosh_history")
            .unwrap();


        // line.push('\n');
        match file.write((line).as_ref()) {
            Err(why) => panic!("Could not write to file {}", why),
            Ok(_) => {}
        }
        match file.write(("\n").as_ref()) {
            Err(why) => panic!("Could not write to file {}", why),
            Ok(_) => {}
        }
    }
}